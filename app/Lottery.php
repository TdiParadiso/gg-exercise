<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lottery extends Model {

	/**
	 * @var array
	 */
	protected $fillable = ['name', 'slug'];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function tickets() {
		return $this->hasMany(LotteryTicket::class);
	}
}
