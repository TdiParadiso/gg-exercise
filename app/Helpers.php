<?php

use App\Lottery;
use Illuminate\Database\Eloquent\Collection;

if (!function_exists('getClosureForLotteryTicketsSearch')) {

	/**
	 * @return Closure
	 */
	function getClosureForLotteryTicketsSearch() {

		/**
		 * @param string        $slug
		 * @param DateTime|null $date
		 *
		 * @return Collection
		 */
		return function (string $slug, \DateTime $date = null) {
			/** @var Lottery|null $lottery */
			$lottery = Lottery::where('slug', $slug)->first();

			if (!$date) {
				$date = new \DateTime();
				$date->modify('-1 month');
			}

			if ($lottery) {
				$tickets = $lottery->tickets()->get();
				$tickets = $tickets->filter(function (\App\LotteryTicket $lotteryTicket) use ($date) {
					return $lotteryTicket->created_at >= $date;
				});
			} else {
				$tickets = collect([]);
			}

			return $tickets;
		};
	}
}
