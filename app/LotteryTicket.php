<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LotteryTicket extends Model {

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function lottery() {
		return $this->belongsTo(Lottery::class);
	}
}
