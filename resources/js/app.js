require('./bootstrap');

$(document).ready(function() {
	initializeJqueryTimer();
});

/**
 * <div class="card-body jquery-timer"
 * 		data-timer-days="1"
 * 		data-timer-hours="2"
 * 		data-timer-minutes="3"
 * 		data-timer-seconds="4">
 * </div>
 */
function initializeJqueryTimer() {
	let $timers = $('.jquery-timer');
	let $mainTemplate = $('#jquery-timer-template');

	$timers.each(function(index) {
		let $this = $(this);
		let isInitialized = false;

		let timesValues = {
			d: parseInt(this.dataset['timerDays']),
			h: parseInt(this.dataset['timerHours']),
			m: parseInt(this.dataset['timerMinutes']),
			s: parseInt(this.dataset['timerSeconds'])
		};

		let timeToEnd = timesValues.s + (timesValues.m * 60) + (timesValues.h * 360) + (timesValues.d * 360 * 24);

		let $template = $mainTemplate.clone();
		$this.append($template.html());

		let $templateFields = {
			'd': $this.find('.jquery-timer-days .jquery-timer-time'),
			'h': $this.find('.jquery-timer-hours .jquery-timer-time'),
			'm': $this.find('.jquery-timer-minutes .jquery-timer-time'),
			's': $this.find('.jquery-timer-seconds .jquery-timer-time'),
		};


		// Main clock interval
		let interval =  setInterval(function() {
			if (timeToEnd > 0) {

				// Calculate time for each unit
				timesValues.s--;
				if (timesValues.m >= 0 && timesValues.s < 0) {
					timesValues.m--;
					timesValues.s = 59;
				}

				if (timesValues.h >= 0 && timesValues.m < 0) {
					timesValues.h--;
					timesValues.m = 59;
				}

				if (timesValues.h < 0) {
					timesValues.h = 23;

					if (timesValues.d > 0) {
						timesValues.d--;
					}
				}

				timeToEnd--;

				updateClockData(timesValues, $templateFields);

				if (!isInitialized) {
					$this.find('.jquery-timer-wrapper').show();
					isInitialized = true;
				}
			} else {
				clearInterval(interval);
			}
		}, 1000);
	});

	function updateClockData(timeValues, $templateFields) {
		let tempValues

		if (timeValues.d) {
			$templateFields['s'].parent().hide();
			$templateFields['d'].parent().show();
		} else {
			$templateFields['s'].parent().show();
			$templateFields['d'].parent().hide();
		}

		for (let key in timeValues) {
			$templateFields[key].text(timeValues[key]);
		}
	}
}
