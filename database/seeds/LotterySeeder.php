<?php

use Illuminate\Database\Seeder;

class LotterySeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		factory(App\Lottery::class, 2)->create()->each(function(App\Lottery $lottery) {
			$lottery->tickets()->save(factory(App\LotteryTicket::class)->make());
		});
	}
}
