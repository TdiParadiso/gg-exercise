<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Lottery;
use Faker\Generator as Faker;

$factory->define(Lottery::class, function (Faker $faker) {
	$name = $faker->words(3, true);
	$slug = str_replace(' ', '-', strtolower($name));
	return [
		'name' => $name,
		'slug' => $slug,
		'timezone' => $faker->timezone,
	];
});
